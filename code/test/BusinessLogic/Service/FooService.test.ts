import { FooService } from "../../../BusinessLogic/Service/FooService";
import { FooDataAccess } from "../../../DataAccess/Repository/FooDataAccess";
import { jest } from "@jest/globals";

const fooDataAccess = new FooDataAccess()
const fooService = new FooService(fooDataAccess);

describe('FooService Test', () => {
    it('Should get all foo', () => {
        const mockFunction = jest.spyOn(fooDataAccess, 'getAll');
        mockFunction.mockReturnValue([{id : 23, message : 'test1'}, {id : 42, message : 'test2'}]);

        const result = fooService.getAllFoo();

        expect(result).toHaveLength(2);
        expect(result[0]).toStrictEqual({id : 23, message : 'test1'});
        expect(result[1]).toStrictEqual({id : 42, message : 'test2'});
    })

    it('Should get a by id', () => {
        const mockFunction = jest.spyOn(fooDataAccess, 'getById');
        mockFunction.mockReturnValue({id : 1, message : 'test1'});

        const result = fooService.getById(1);
        
        expect(result).toStrictEqual({id : 1, message : 'test1'});
    })
})