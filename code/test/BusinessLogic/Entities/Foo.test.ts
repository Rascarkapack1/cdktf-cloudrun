import { Foo } from "../../../BusinessLogic/Entities/Foo";

describe('Foo entity', () => {
it('Should create new instance of Foo', () => {
    const foo = new Foo();
    foo.id = 1;
    foo.message = 'test';

    expect(foo).toBeInstanceOf(Foo);
    expect(foo).toHaveProperty('id', 1);
    expect(foo).toHaveProperty('message', 'test');
})
})