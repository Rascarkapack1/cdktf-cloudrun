import { Foo } from "../BusinessLogic/Entities/Foo"

const mock : Array<Foo> = [{
    id : 1,
    message : "ca marche au top"
},
    {
        id : 2,
        message : "C'est la 2ème"
},
    {
    id : 3,
    message : "Jamais 2 sans 3"
}]

export default mock;