# 🎉CDKTF CloudRun Example🎉

## 💬 Contexte
Simple template in TypeScript with Express.
It's just a exemple/starter for project to build , run and create a cloud run service based with a docker Image.

## 🔧 Install

`git clone` this repo

then :

`npm install -g cdktf-cli`
`npm install -g typescript`

cd ./code : 
`npm i`

then cd ./cloud-deploy :
`npm i`
`npm run get`

and done

## Deploy

You will need service account credentials json file of your project.

Copy it on cloud-deploy folder and update your variables/config.dev.ts
