#!/usr/bin/env bash
if [ ! -z "$CI_COMMIT_TAG" ]
then
    echo "{\"value\": \"$CI_COMMIT_TAG\"}"
elif [ ! -z "$CI_COMMIT_REF_SLUG" ]
then
    echo "{\"value\": \"$CI_COMMIT_REF_SLUG\"}"
else
    echo "{\"value\": \"$(git symbolic-ref --short HEAD | sed -e "s/\//-/g" | sed -e "s/\./-/g" | sed -e 's/\(.*\)/\L\1/')\"}"
fi

