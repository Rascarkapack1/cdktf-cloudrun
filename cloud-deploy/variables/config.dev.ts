class Params {
    projectName : string;
    region : string;
    projectId : string;
    pathToCredentials: string;    

    constructor(projectName : string, region : string, projectID : string, pathToCredentials : string) {
        this.projectName = projectName;
        this.region = region;
        this.projectId = projectID;
        this.pathToCredentials = pathToCredentials;
    }
}

const variables = new Params('testcdktf', 'europe-west1', 'projectId', 'credentials')


export default variables