import { exec } from 'child_process';

const gitTag = async function () {
    return new Promise( (resolve, reject) => {
        exec(
            "sh git_get_tag.sh",
            (error, stdout, stderr) => {
                if(error) reject(error)
                resolve(stdout);
            }
        );
    }) 

   
};


export {gitTag}