import {exec} from "child_process";

const gitBranch = async function () {
    return new Promise( (resolve, reject) => {
        exec(
            "sh git_branch.sh",
            {cwd: `${process.cwd()}/feature`},
            (error, stdout, stderr) => {
                if(error) reject(error)
                resolve(stdout);
            }
        );
    }) 

   
};
export {gitBranch};
