import { Construct } from "constructs";
import { TerraformOutput, TerraformStack } from "cdktf";
import {
  CloudRunService,
  GoogleProvider,
  DataGoogleIamPolicy,
  CloudRunServiceIamPolicy,
  StorageBucket,
  StorageBucketObject
  
} from "../.gen/providers/google";
import { Resource, NullProvider } from "../.gen/providers/null";
import { DataArchiveFile, ArchiveProvider } from "../.gen/providers/archive";
import path = require("path");
import * as fs from "fs"
import variables from "../variables/config.dev";

class FeatureStack extends TerraformStack {
  constructor(scope: Construct, name: string, config : {feature_suffix : string, image_tag : string}) {
    super(scope, name);
    const  feature_suffix = config.feature_suffix;
    const image_tag = config.image_tag;

    //PROVIDERS ZONE
    new NullProvider(this, "null");
    new ArchiveProvider(this, "archive");

    const credentialsPath = path.join(process.cwd(), variables.pathToCredentials);
    const credentials = fs.existsSync(credentialsPath)
      ? fs.readFileSync(credentialsPath).toString()
      : "{}";

    new GoogleProvider(this, "GoogleAuth", {
      region: variables.region,
      zone: variables.region + "-c",
      project: variables.projectId,
      credentials,
    });

    const archiveCode = new DataArchiveFile(this, 'codeArchive', {
      type : 'zip',
      sourceDir :'../../../../code',
      outputPath : `./${variables.projectName}.zip`
    })

    const codeBucket = new StorageBucket(this, 'codeBucket', {
      location : variables.region,
      name : `bucket-${variables.projectName}${config.feature_suffix}`,
      storageClass : 'REGIONAL',
      forceDestroy : true
    })

    const codeBucketObject = new StorageBucketObject(this, 'codeBucketObject', {
      dependsOn : [codeBucket],
      bucket : codeBucket.name,
      name : `${variables.projectName}-${archiveCode.outputMd5}.zip`,
      source : archiveCode.outputPath
    })

    const nullRessource = new Resource(this, "testLocalExec", {
      dependsOn : [codeBucketObject],
      triggers : { on : codeBucketObject.md5Hash} 
    });
    nullRessource.addOverride('provisioner', [
      {"local-exec" : { command: `gcloud builds submit '${codeBucket.url}/${codeBucketObject.name}' --config ../../../../code/cloudbuild.yaml --substitutions TAG_NAME=${image_tag}-${archiveCode.outputMd5},_BRANCH='${feature_suffix}' --gcs-log-dir='gs://${variables.projectId}_cloudbuild/logs' --gcs-source-staging-dir='gs://${variables.projectId}_cloudbuild/source'`},},
    ])

    // define resources here
    const imagename = `eu.gcr.io/${variables.projectId}/${variables.projectName}:${image_tag}-${archiveCode.outputMd5}`;    

    const cloudrunsvcapp = new CloudRunService(this, "GcpCDKCloudrunsvc", {
      dependsOn : [nullRessource],
      location: variables.region,
      name: `cloudrun${feature_suffix}`,
      template: {
        spec: {
          containers: [
            {
              image: imagename,
            },
          ],
        },
      },
    });

    const policy_data = new DataGoogleIamPolicy(this, "datanoauth", {
      binding: [
        {
          role: "roles/run.invoker",
          members: ["allUsers"],
        },
      ],
    });

    new CloudRunServiceIamPolicy(this, "runsvciampolicy", {
      location: variables.region,
      project: cloudrunsvcapp.project,
      service: cloudrunsvcapp.name,
      policyData: policy_data.policyData,
    });

    new TerraformOutput(this, "cdktfcloudrunUrl", {
      value: cloudrunsvcapp.status.get(0).url,
    });

    new TerraformOutput(this, "cdktfcloudrunUrlN", {
      value: cloudrunsvcapp.status.get(0).url,
    });
  }
}



export {FeatureStack};
